﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Punto1.Core.DTOs;
using Punto1.Core.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Punto1.WebApp.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class Punto1Controller : ControllerBase
    {
        private IApp _App;
        public Punto1Controller(IApp app)
        {
            _App = app;
        }
        [HttpPost]
        public IActionResult Post([FromBody] RequestDTO request)
        {
            ResponseDTO response = _App.DeterminarEstado(request);
            return Ok(response);
        }
    }
}
