﻿using Punto1.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto1.Core.Intefaces
{
    public interface IApp
    {
        ResponseDTO DeterminarEstado(RequestDTO data);
    }
}
