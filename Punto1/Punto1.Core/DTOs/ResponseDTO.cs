﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto1.Core.DTOs
{
    public class ResponseDTO
    {
        public int dias { get; set; }
        public int[] entrada { get; set; }
        public int[] salida { get; set; }
    }
}
