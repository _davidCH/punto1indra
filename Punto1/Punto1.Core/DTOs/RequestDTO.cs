﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto1.Core.DTOs
{
    public class RequestDTO
    {
        public int[] lstCasas { get; set; }
        public int dias { get; set; }
    }
}
