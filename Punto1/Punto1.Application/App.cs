﻿using Punto1.Core.DTOs;
using Punto1.Core.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto1.Application
{
    public class App : IApp
    {
        public ResponseDTO DeterminarEstado(RequestDTO data)
        {
            int[] AuxlstCasas = new int[data.lstCasas.Length];
            int Longitud = data.lstCasas.Length;
            bool Ambos_vecinos_en_mismo_estado;
            ResponseDTO result = new ResponseDTO();
            result.dias = data.dias;
            result.entrada = new int[data.lstCasas.Length];
            result.salida = new int[data.lstCasas.Length];

            data.lstCasas.CopyTo(result.entrada,0);
            data.lstCasas.CopyTo(result.salida,0);

            for (var i=0; i<data.dias;i++)
            {
                AuxlstCasas[0] = (result.salida[1] == 0) ?0:1;
                AuxlstCasas[Longitud- 1] = (result.salida[Longitud - 2] == 0) ? 0 : 1;
                for (var j=1;j< AuxlstCasas.Length - 1;j++)
                {
                    Ambos_vecinos_en_mismo_estado = result.salida[j - 1] == result.salida[j + 1];
                    AuxlstCasas[j] = (Ambos_vecinos_en_mismo_estado) ?0:1;
                }
                AuxlstCasas.CopyTo(result.salida,0);
            }

            return result;
        }
    }
}
